import Store from './js/Store'
import { createTodo } from './js/Todo'
import { FILTER_ALL, FILTER_OPEN, FILTER_CLOSED } from './js/Filters'
import {
  CHECKBOX_CLASS,
  EDIT_BUTTON_CLASS,
  DELETE_BUTTON_CLASS,
  FILTER_CLASS_ALL,
  FILTER_CLASS_OPEN,
  FILTER_CLASS_CLOSED,
  EDIT_TITLE_CLASS,
  EDIT_TODO_FORM_CLASS,
  CREATE_TODO_FORM_CLASS,
  renderTodos
} from './js/Renderers'
import { getStorageState, setStorageState } from './js/LocalStorageProvider'

import './css/app.css'

function setActiveFilter (filter) {
  Store.setState({
    activeFilter: filter
  })
}

function addTodo (title) {
  Store.setState({
    todos: [...Store.getState().todos, createTodo(title)]
  })
}

function toggleTodoStatus (id) {
  Store.setState({
    todos: Store.getState().todos.map(todo => {
      if (!(todo.id === id)) return todo
      return (todo.isClosed) ? todo.open() : todo.close()
    })
  })
}

function editTodo (id) {
  const { todos, isEditing } = Store.getState()
  if (isEditing) {
    return
  }
  const idx = todos.findIndex((todo) => todo.id === id)
  Store.setState({
    todos: [
      ...todos.slice(0, idx),
      todos[idx].edit(),
      ...todos.slice(idx + 1)
    ],
    isEditing: true
  })
}

function saveTodo (id, newTitle) {
  const todos = Store.getState().todos
  const idx = todos.findIndex((todo) => todo.id === id)
  Store.setState({
    todos: [
      ...todos.slice(0, idx),
      todos[idx].setTitle(newTitle).save(),
      ...todos.slice(idx + 1)
    ],
    isEditing: false
  })
}

function removeTodo (id) {
  Store.setState({
    todos: Store.getState().todos.filter(todo => todo.id !== id)
  })
}

function renderApp (rootElement, state) {
  rootElement.innerHTML = renderTodos(state)
  const focusElement = document.querySelector(`.${EDIT_TITLE_CLASS}`)
  console.log(focusElement)
  if (focusElement) {
    focusElement.focus()
    focusElement.select()
  } else {
    document.querySelector('.todos__input').focus()
  }
}

// initialize store
Store.setState(getStorageState() || {
  activeFilter: FILTER_ALL,
  todos: [
    createTodo('my first todo'),
    createTodo('my second todo'),
    createTodo('my third todo'),
    createTodo('my fourth todo')
  ]
})

Store.subscribe(setStorageState)

const root = document.createElement('div')
root.id = 'vanillajs-todo'
root.classList.add('vanillajs-todo')
document.body.appendChild(root)

Store.subscribe(state => renderApp(root, state))

document.addEventListener('change', event => {
  if (!event.target.classList.contains(CHECKBOX_CLASS)) return
  toggleTodoStatus(Number(event.target.dataset.todoId))
})

document.addEventListener('click', event => {
  const classList = event.target.classList
  const id = Number(event.target.dataset.todoId)

  if (classList.contains(EDIT_BUTTON_CLASS)) {
    editTodo(id)
  }

  if (classList.contains(DELETE_BUTTON_CLASS)) {
    removeTodo(id)
  }

  if (classList.contains(FILTER_CLASS_ALL)) {
    setActiveFilter(FILTER_ALL)
  }

  if (classList.contains(FILTER_CLASS_OPEN)) {
    setActiveFilter(FILTER_OPEN)
  }

  if (classList.contains(FILTER_CLASS_CLOSED)) {
    setActiveFilter(FILTER_CLOSED)
  }
})

document.addEventListener('submit', event => {
  const classList = event.target.classList
  const id = Number(event.target.dataset.todoId)

  if (classList.contains(CREATE_TODO_FORM_CLASS)) {
    addTodo(event.target.querySelector('input').value.trim())
  }

  if (classList.contains(EDIT_TODO_FORM_CLASS)) {
    saveTodo(id, String(document.querySelector(`#edit-todo-${id}`).value.trim()))
  }

  event.preventDefault()
})

renderApp(root, Store.getState())
