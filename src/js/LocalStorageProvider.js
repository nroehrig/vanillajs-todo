import { createTodoFromObject } from './Todo'

const LOCAL_STORAGE_NAME = 'vanillajs-todo'

export function getStorageState () {
  if (!window.localStorage) return
  const storageItem = window.localStorage.getItem(LOCAL_STORAGE_NAME)
  if (!storageItem) return
  const data = JSON.parse(storageItem)
  return {
    activeFilter: data.activeFilter,
    todos: data.todos.map(todo => createTodoFromObject(todo))
  }
}

export function setStorageState (state) {
  if (!window.localStorage) return
  const storageObject = {
    activeFilter: state.activeFilter,
    todos: state.todos.map(({ title, isClosed, isEditing, reference }) => {
      return {
        title: (isEditing) ? reference.title : title,
        isClosed
      }
    })
  }

  window.localStorage.setItem(LOCAL_STORAGE_NAME, JSON.stringify(storageObject))
}
