let currentId = 0

class Todo {
  constructor (title, isClosed, isEditing, reference) {
    this.title = title
    this.isClosed = isClosed || false
    this.isEditing = isEditing || false
    this.reference = reference
    this.id = ++currentId
    return Object.freeze(this)
  }

  setTitle (newTitle) {
    return new Todo(String(newTitle), this.isClosed, this.isEditing)
  }

  close () {
    return new Todo(this.title, true)
  }

  open () {
    return new Todo(this.title, false)
  }

  edit () {
    return new Todo(this.title, this.isClosed, true, this)
  }

  save () {
    return new Todo(this.title, this.isClosed, false)
  }
}

export function createTodo (title) {
  return new Todo(title)
}

export function createTodoFromObject (todoObject) {
  return new Todo(todoObject.title, todoObject.isClosed)
}

export function isTodo (todo) {
  return todo instanceof Todo
}
