const history = []
let subscribers = []
let state = Object.freeze({})

export default {
  setState (newState) {
    history.push(state)
    state = Object.freeze(Object.assign({}, state, newState))
    subscribers.forEach(subscriber => subscriber(state))
  },

  getState () {
    return state
  },

  subscribe (subscriber) {
    if (typeof subscriber !== 'function') return
    subscribers = [...subscribers, subscriber]
  },

  unsubscribe (subscriber) {
    subscribers = subscribers.filter(item => item !== subscriber)
  }
}
