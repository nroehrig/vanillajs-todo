import escape from 'lodash.escape'

import { isTodo } from './Todo'
import { FILTER_ALL, FILTER_CLOSED, FILTER_OPEN } from './Filters'

import iconEdit from '../img/icon-edit.svg'
import iconDelete from '../img/icon-delete.svg'
import iconSave from '../img/icon-save.svg'

export const CHECKBOX_CLASS = 'todo__checkbox'
export const EDIT_BUTTON_CLASS = 'todo__edit'
export const SAVE_BUTTON_CLASS = 'todo__save'
export const DELETE_BUTTON_CLASS = 'todo__delete'
export const FILTER_CLASS_ALL = 'filter--all'
export const FILTER_CLASS_OPEN = 'filter--open'
export const FILTER_CLASS_CLOSED = 'filter--closed'
export const EDIT_TITLE_CLASS = 'todo__edit-title'
export const EDIT_TODO_FORM_CLASS = 'todo__form'
export const CREATE_TODO_FORM_CLASS = 'todos__creator'

function renderTitle (todo) {
  const { id, title, isEditing } = todo
  if (isEditing) {
    return `<input type="text" id="edit-todo-${id}" class="${EDIT_TITLE_CLASS}" data-todo-id="${id}" value="${escape(title)}">`
  } else {
    return `<label for="todo-checkbox-${id}" class="todo__title">${escape(title)}</label>`
  }
}

function renderButtons (todo) {
  const { id, isEditing } = todo
  const action = (isEditing)
    ? `<button type="submit" class="button button--icon button--save ${SAVE_BUTTON_CLASS}" data-todo-id="${id}" title="Save todo"><img src="${iconSave}" alt="Save todo" class="icon icon--save"></button>`
    : `<button type="button" class="button button--icon button--edit ${EDIT_BUTTON_CLASS}" data-todo-id="${id}" title="Edit todo"><img src="${iconEdit}" alt="Edit todo" class="icon icon--edit"></button>`
  return `
    ${action}
    <button type="button" class="button button--icon button--delete ${DELETE_BUTTON_CLASS}" data-todo-id="${id}" title="Delete todo"><img src="${iconDelete}" alt="Delete todo" class="icon icon--delete"></button>
  `
}

function renderTodo (todo) {
  if (!isTodo(todo)) return ''
  const { isClosed, id } = todo

  return `
    <form class="${EDIT_TODO_FORM_CLASS}" method="post" action="#" data-todo-id="${id}">
      <div class="todo${isClosed ? ' todo--closed' : ''}" id="todo-${id}">
        <input type="checkbox" id="todo-checkbox-${id}" data-todo-id=${id} class="${CHECKBOX_CLASS}"${isClosed ? ' checked' : ''}>
        ${renderTitle(todo)}
        <div class="todo__buttons">
          ${renderButtons(todo)}
        </div>
      </div>
    </form>
  `
}

function renderFilters (activeFilter, extraClasses = []) {
  const filtersClasses = ['filters', ...extraClasses]
  const baseFilterClasses = ['button', 'button--filter', 'filter']
  const allClasses = [...baseFilterClasses, FILTER_CLASS_ALL, FILTER_ALL === activeFilter ? ' filter--active' : '']
  const openClasses = [...baseFilterClasses, FILTER_CLASS_OPEN, FILTER_OPEN === activeFilter ? ' filter--active' : '']
  const closedClasses = [...baseFilterClasses, FILTER_CLASS_CLOSED, FILTER_CLOSED === activeFilter ? ' filter--active' : '']

  return `
    <div class="${filtersClasses.join(' ')}">
      <button type="button" class="${allClasses.join(' ')}">
        all
      </button>
      <button type="button" class="${openClasses.join(' ')}">
        open
      </button>
      <button type="button" class="${closedClasses.join(' ')}">
        closed
      </button>
    </div>
  `
}

function filterTodos (todos, activeFilter) {
  return todos.filter(todo => {
    if (activeFilter === FILTER_ALL) return true
    if (activeFilter === FILTER_CLOSED && todo.isClosed) return true
    if (activeFilter === FILTER_OPEN && !todo.isClosed) return true
    return false
  })
}

export function renderTodos (state) {
  const toBeDisplayed = filterTodos(state.todos, state.activeFilter)
  return `
    <div>
      <div class="todos">
        <header class="todos__header">
          <h1 class="todos__headline">VanillaJS - Todo</h1>
        </header>
        <form class="${CREATE_TODO_FORM_CLASS}" method="post" action="#">
          <input type="text" placeholder="Your todo here..." class="todos__input" autofocus required>
          <button type="submit" class="button button--create todos__submit">Add new</button>
        </form>
        <ul class="todos__list">
        ${toBeDisplayed.reduce((acc, todo) => acc + renderTodo(todo), '')}
        </ul>
        ${renderFilters(state.activeFilter, ['todos__filters'])}
      </div>
      <footer class="todos__footer footer">
        <a class="footer__link" href="https://thenounproject.com/search/?q=delete&i=1042028">Delete by myladkings from the Noun Project</a><br>
        <a class="footer__link" href="https://thenounproject.com/search/?q=edit&collection=44164&i=1471032">edit by myladkings from the Noun Project</a><br>
        <a class="footer__link" href="https://thenounproject.com/search/?q=save&i=1203125">Save by Pundimon from the Noun Project</a>
      </footer>
    </div>
  `
}
