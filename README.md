# vanilla.js Todo App #

Simple todo app saving to localstorage using no UI framework and little dependencies.

Created to exercise my skills in JS.

### How do I get set up? ###

Requirements: node 8

1. clone this repo
2. `npm install`
3. `npm start`